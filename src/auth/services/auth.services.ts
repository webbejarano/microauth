import { injectable } from "inversify-props";
import { ClientRepository } from "../repositories/client.repository";
import { getCustomRepository, InsertResult } from "typeorm";
import { Clients } from "../entity/clients";
import { comparePassword, makePassword } from "../../../core/tools/hash";
import throwFactoryException from "../../../core/http/exceptions";
import { HttpStatus } from "../../../core/http/HttpStatus";
import { parseDateYYYYMMDD } from "../../../core/tools/date";
import { BuildException } from "../../../core/http/exceptions/build.exception";

@injectable()
export class AuthServices {
  private clientRepository = getCustomRepository(ClientRepository);

  async login(email: string, password: string): Promise<Clients> {
    const client = await this.clientRepository.findByEmail(email);

    if (!(client instanceof Clients)) {
      throwFactoryException(
        "Cliente no está registrado.",
        "No fue encontrado en el sistema.",
        HttpStatus.BAD_REQUEST
      );
    }
    if (await comparePassword(password, client.password)) {
      throwFactoryException(
        "Cliente no está registrado.",
        "La contraseña ingresada no es correcta.",
        HttpStatus.BAD_REQUEST
      );
    }
    delete client.password;
    return client;
  }

  async getAll(): Promise<Clients | null> {
    return await this.clientRepository.findOne(1);
  }

  async signUp(client: Clients): Promise<Clients> {
    if (await this.existsClient(client.email)) {
      Promise.reject<BuildException>(
        throwFactoryException(
          "Fué imposible procesar su registrado.",
          "El correo fué registrado por otro cliente.",
          HttpStatus.BAD_REQUEST
        )
      );
    }
    if (client.date_birth.toString().length > 0) {
      client.date_birth = await parseDateYYYYMMDD(client.date_birth.toString());
    }

    client.password = await makePassword(client.password);

    await this.clientRepository.insert(client);
    return client;
  }

  async existsClient(email: string): Promise<boolean> {
    const client = await this.clientRepository.findByEmail(email);
    return Promise.resolve(client instanceof Clients);
  }
}
