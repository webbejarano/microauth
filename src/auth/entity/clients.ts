import { Column, Entity, ManyToOne, PrimaryGeneratedColumn } from "typeorm";
import { Dealers } from "./dealers";

@Entity()
export class Clients {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({
    length: 40,
    type: "character varying",
  })
  first_name: string;

  @Column({
    length: 40,
    type: "character varying",
  })
  last_name: string;

  @Column({
    length: 100,
    type: "character varying",
  })
  email: string;

  @Column({
    length: 20,
    type: "character varying",
  })
  phone_number: string;

  @Column({
    unique: true,
    length: 14,
    type: "character varying",
  })
  identify_number: string;

  @Column()
  date_birth: Date;

  @Column({
    default: true,
  })
  is_abroad: boolean;

  @Column({
    length: 14,
    type: "character varying",
  })
  gender: string;

  @Column({
    length: 20,
    type: "character varying",
  })
  password: string;

  /*@ManyToOne(() => Dealers, (dealer) => dealer.id)
  dealer: Dealers;*/
}
