import { Column, Entity, OneToMany, PrimaryGeneratedColumn } from "typeorm";
import { Clients } from "./clients";

@Entity()
export class Dealers {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({
    length: 40,
    type: "character varying",
  })
  state: string;

  @Column({
    length: 40,
    type: "character varying",
  })
  city: string;

  @Column({
    length: 100,
    type: "character varying",
  })
  email: string;

  @Column({
    length: 20,
    type: "character varying",
  })
  phone_number: string;

  @Column({
    unique: true,
    length: 100,
    type: "character varying",
  })
  business: string;

  @OneToMany(() => Clients, (client) => client.id)
  clients: Clients;
}
