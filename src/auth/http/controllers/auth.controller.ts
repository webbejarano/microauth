import {
  BaseHttpController,
  controller,
  httpPost,
  request,
} from "inversify-express-utils";
import { AuthServices } from "../../services/auth.services";
import responseFactory from "../../../../core/response/response";
import { HttpStatus } from "../../../../core/http/HttpStatus";
import { BuildException } from "../../../../core/http/exceptions/build.exception";
import { AuthRequest } from "../requests/auth.request";
import { SignupRequest } from "../requests/signup.request";

@controller("/api/v1")
export class AuthController extends BaseHttpController {
  private service: AuthServices;
  constructor(service: AuthServices) {
    super();
    this.service = service;
  }

  @httpPost("/auth")
  async auth(@request() req) {
    try {
      const body = await new AuthRequest().handle(req);

      const client = await this.service.login(body.email, body.password);
      return responseFactory("", client);
    } catch (e) {
      if (e instanceof BuildException) {
        return e.render();
      }
    }
    return responseFactory("", null, HttpStatus.INTERNAL_SERVER_ERROR);
  }

  @httpPost("/signup")
  async signUp(@request() req) {
    const body = await new SignupRequest().handle(req);
    return await this.service
      .signUp(body)
      .then((client) => {
        return responseFactory("Welcome", client, HttpStatus.CREATED);
      })
      .catch((e) => {
        if (e instanceof BuildException) {
          return e.render();
        }
        return responseFactory(
          e.detail,
          null,
          HttpStatus.INTERNAL_SERVER_ERROR
        );
      });
  }
}
