import { ValidatorAbstract } from "../../../../core/http/ValidatorAbstract";

export class AuthRequest extends ValidatorAbstract {
  getRules(): any {
    return {
      email: "required|email",
      password: "required|min:8",
    };
  }
}
