import { ValidatorAbstract } from "../../../../core/http/ValidatorAbstract";

export class SignupRequest extends ValidatorAbstract {
  getRules(): any {
    return {
      first_name: "required|max:40",
      last_name: "required|max:40",
      email: "email|required",
      phone_number: "required|max:20",
      identify_number: "required|max:20",
      password: "required|max:20|min:8",
    };
  }
}
