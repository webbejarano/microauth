import { Clients } from "../entity/clients";
import { EntityRepository } from "typeorm";
import { Repository } from "typeorm/repository/Repository";

@EntityRepository(Clients)
export class ClientRepository extends Repository<Clients> {
  async findByEmail(email: string): Promise<Clients | null> {
    return await this.findOne({ email });
  }
  async findByIdentifyNumber(identifyNumber: string): Promise<Clients | null> {
    return await this.findOne(identifyNumber);
  }
}
