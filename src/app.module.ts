import { SingletonModule } from "../core/singleton/singleton.module";
import { AuthController } from "./auth/http/controllers/auth.controller";
import { AuthServices } from "./auth/services/auth.services";

export class AppModule extends SingletonModule {
  imports = [];
  providers = [AuthServices];
  controllers = [AuthController];
}
