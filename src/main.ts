import "reflect-metadata";
import { AppModule } from "./app.module";
import { ServerJaspe } from "../core/server.jaspe";

async function bootstrap() {
  const app = ServerJaspe.factory(new AppModule());
  app.run();
}
bootstrap();
